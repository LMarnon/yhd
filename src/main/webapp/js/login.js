var SCaptcha;
//页面初始化
function pageInit() {
    jsLoginFed.loadFunLogin();
    jsLoginValidatCode.ValidatCodeInit()
}
//检查用户名和密码
function checkAccountAndPwdForLogin() {
    var e = $("#un").val();
    var f = $("#pwd").val();
    if (f == "" && (e == "" || e == "邮箱/手机/用户名")) {
        LoginUtils.showErrorInfo($("#un"), "请输入账号和密码");
        LoginUtils.button_recover();
        return false
    }
    if (e == "" || e == "邮箱/手机/用户名") {
        LoginUtils.showErrorInfo($("#un"), "请输入账号");
        LoginUtils.button_recover();
        return false
    } else {
        if (LoginUtils.stringLen(e) > 100) {
            LoginUtils.showErrorInfo($("#un"), "账号长度不能超过100位");
            LoginUtils.button_recover();
            return false
        } else {
            if (e.toLowerCase().indexOf("<script") > -1 || e.toLowerCase().indexOf("<\/script") > -1) {
                LoginUtils.showErrorInfo($("#un"), "账号中包含非法字符");
                LoginUtils.button_recover();
                return false
            }
        }
    }
    var d = /\s+/;
    if (f == "") {
        LoginUtils.showErrorInfo($("#pwd"), "请输入密码");
        LoginUtils.button_recover();
        return false
    } else {
        if (d.test(f)) {
            LoginUtils.showErrorInfo($("#pwd"), "密码不能有空格");
            LoginUtils.button_recover();
            return false
        }
    }
    return true
}
//登录按钮点击事件
function double_submit() {
    $("#login_button").attr("disabled", "disabled");
    $("#login_button").text("登录中...");
    $("#error_tips").hide();
    if (!checkAccountAndPwdForLogin()) {
        return false
    }
    var k = $("#un").val();
    var j = $("#pwd").val();

    $.ajax({
        type:'post',
        url:"/wx/login",
        data:JSON.stringify({
            user_name:k,
            user_password:j
        }),
        contentType:"application/json;charset=UTF-8",
        dataType: 'json',
        success:function(data) {
            if(data == '0') {
                LoginUtils.showErrorInfo($("#un"), "账号和密码不匹配，请重新输入");
                LoginUtils.button_recover();
            }else {
                alert("登录成功！")
                setTimeout(function () {
                    window.location.href = '../passport/homepage.html?user_name='+ k
                },2000)
            }
        },error:function () {
            LoginUtils.showErrorInfo($("#un"), "请检查网络");
            LoginUtils.button_recover();
        }
    })
};
var jsLoginFed = {
    helpCenterHover: function () {
        $(".help_wrap", ".regist_header_right ").hover(function () {
            $(this).addClass("help_wrap_hover")
        }, function () {
            $(this).removeClass("help_wrap_hover")
        })
    },
    registForm: function (c) {
        var d = $(c).val();
        //用delegate()为c添加focus事件
        $(".login_form").delegate(c, "focus", function () {
            if ($(this).val() == "邮箱/手机/用户名" || $(this).val() == "验证码") {
                $(this).val("")
            }
            $(this).removeClass("gay_text");
            $(this).parents(".form_item").addClass("cur")
        });
        $(".login_form").delegate(c, "blur", function () {
            var a = $(this).val();
            if (!a) {
                $(this).val(d).addClass("gay_text")
            }
            $(this).parents(".form_item").removeClass("cur")
        })
    },
    //用户名input聚焦失焦事件
    focus_un: function () {
        $("#un").live("click keydown", function () {
            if ($("#un").val() == "邮箱/手机/用户名") {
                $("#un").val("");
                $("#un").focus()
            }
        });
        $("#un").mouseover(function () {
            $("#un").focus()
        });
        $("#un").mouseout(function () {
            if (!$("#un").val()) {
                $("#un").val("邮箱/手机/用户名");
                $("#un").addClass("gay_text")
            }
        })
    },
    serviceAgreement: function () {
        $("#check_agreement").click(function () {
            if ($(this).hasClass("uncheck_agreement")) {
                $(this).attr("class", "check_agreement");
                $("#isAutoLogin").attr("value", "1");
                $("#agreement_tips").show()
            } else {
                $(this).attr("class", "uncheck_agreement");
                $("#isAutoLogin").attr("value", "0");
                $("#agreement_tips").hide()
            }
            return false
        })
    },
    jointLanding: function () {
        var b = $(".joint_landing_wrap");
        $("li:gt(3)", b).hide();
        b.delegate(".unfold", "click", function () {
            var a = $(this);
            if (a.hasClass("fold")) {
                $("li:gt(3)", b).hide();
                $(this).removeClass("fold")
            } else {
                $("li:gt(3)", b).show();
                $(this).addClass("fold")
            }
        })
    },
    doEnter: function () {
        $("#pwd,#vcd,#login_button").keydown(function (b) {
            if (b.keyCode == 13) {
                if (jQuery.browser.msie && jQuery.browser.version == "6.0") {
                    $("#login_button").trigger("click")
                } else {
                    $("#login_button").click()
                }
            }
        })
    },
    //登录页加载
    loadFunLogin: function () {
        jsLoginFed.helpCenterHover();
        jsLoginFed.registForm(".ipt_username");
        jsLoginFed.registForm(".ipt_password");
        jsLoginFed.serviceAgreement();
        jsLoginFed.jointLanding();
        jsLoginFed.doEnter();
        jsLoginFed.focus_un();
    }
}
var jsLoginValidatCode = {
    accountValueInit: function (d) {
        var c = jsLoginValidatCode.getCookie("ac");
        if (c) {
            d.val(decodeURIComponent(c));
            $(".ipt_username").removeClass("gay_text");
            $("#pwd").focus();
        } else {
            if (d.val() == "") {
                d.val("邮箱/手机/用户名");
                d.focus();
                $("#un").addClass("gay_text")
            }
        }
        return d.val()
    },
    getCookie: function (h) {
        var g = document.cookie.split(";");
        for (var e = 0; e < g.length; e++) {
            var f = g[e].split("=");
            if (f[0].replace(/(^\s*)|(\s*$)/g, "") == h) {
                return f[1]
            }
        }
        return ""
    },
    ValidatCodeInit: function () {
        jsLoginValidatCode.initSlideCaptcha();
        jsLoginValidatCode.accountValueInit($("#un"));
    },
    initSlideCaptcha: function () {
        var b = {
            env: "pc-login",
            anchorBtn: "login_button",
            checkBeforeShow: function () {
                return checkAccountAndPwdForLogin()
            },
            onValidateSuccess: function () {
                double_submit()
            }
        };
        SCaptcha = new SlideCaptcha(b)
    }
}
var LoginUtils = {
    stringLen: function (c) {
        c = LoginUtils.stringTrim(c);
        var d = 0;
        if (c) {
            d = c.replace(/[^\x00-\xff]/g, "***").length
        }
        return d
    },
    stringTrim: function (b) {
        return b.replace(/(^\s*)|(\s*$)/g, "")
    },
    showPreRegisterInfo: function (d, c) {
        $("#error_tips").html(c);
        $("#error_tips").show();
        if (d != null) {
            d.focus()
        }
    },
    showErrorInfo: function (d, c) {
        $("#error_tips").text(c);
        $("#error_tips").show();
        if (d != null) {
            d.focus()
        }
    },
    clearErrorInfo: function () {
        $("#error_tips").text("");
        $("#error_tips").hide()
    },
    button_recover: function () {
        $("#login_button").removeAttr("disabled");
        $("#login_button").text("登录")
    }
}
