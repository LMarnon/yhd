var timer1 = null;
var timer2 = null;
var SCaptcha;
var phoneCode = "";
(function (b) {
        if (typeof define === "function" && define.amd) {
            define(["zepto"], b)
        } else {
            window.SlideCaptcha = b(jQuery)
        }
    }(function (n) {
            var m = false;
            var k = false;
            var o = "";

            function p(a) {
            }

            function i(a) {
                a.hidePicCaptcha();
                if (a.isSlide() && a.beforeCheck()) {
                    k = true;
                    window.jab && window.jab.showCaptcha({
                        onValidateSuccess: function (b) {
                            k = false;
                            o = b;
                            a.onValidateSuccess(b)
                        },
                        onValidateFail: function () {
                            o = ""
                        },
                        onClose: function () {
                            o = ""
                        },
                        requestId: j("jab-requestId")
                    })
                }
            }

            function j(e) {
                String.prototype.trim = function () {
                    return this.replace(/^\s/g, "")
                };
                var b = {};
                var c = document.cookie || "";
                var g = c.split(";");
                for (var a = 0, d = g.length; a < d; a++) {
                    var f = g[a].split("=");
                    b[f[0].trim()] = f[1].trim()
                }
                return b[e] || ""
            }

            function l(a) {
                if (!(this instanceof l)) {
                    return new l(a)
                }
                this.env = a.env;
                this.registBtn = a.registBtn;
                this.beforeCheck = a.beforeCheck;
                this.hidePicCaptcha = a.hidePicCaptcha;
                this.onValidateSuccess = a.onValidateSuccess;
                this.init();
                this.disSlide()
            }

            l.prototype = {
                constructor: l,
                init: function () {
                    p(this)
                },
                show: function () {
                    i(this)
                },
                setSlide: function () {
                    m = true
                },
                isSlide: function () {
                    return m
                },
                disSlide: function () {
                    m = false
                },
                isShowing: function () {
                    return k
                },
                getData: function () {
                    return o
                }
            };
            return l
        }
    )
);
var jRegist = {
    //初始化
    init: function () {
        jRegist.focusInput();
        jRegist.capsLock();
        jRegist.tipsShowHide();
        jRegist.pswdShow();
        jRegist.loadingBtn();
        jRegist.messageBind();
        jRegist.helpCenterHover();
        jRegist.init_slide_captcha()
    },
    show_slide_captcha: function () {
        SCaptcha.setSlide();
        SCaptcha.show();
        sms.change_state_disable()
    },
    //input框聚集
    focusInput: function () {
        $(".ysame_input").focus(function () {
            var b = $(this).next(".y_same_label"),
                a = b.outerWidth(true);
            b.animate({
                left: -a - 20
            }, 160)
        })
    },
    //设置密码框
    capsLock: function (b) {
        var a = false;
        $(document).keydown(function (c) {
            if (c.keyCode == 20) {
                if (a == false) {
                    a = true;
                    $(".y_set_password").addClass("caplock_open")
                } else {
                    a = false;
                    $(".y_set_password").removeClass("caplock_open")
                }
            }
        })
    },
    checkStrong: function (b) {
        var a = checkPassword.getPwdPoint($(b).val());
        if (a > 80) {
            return 3
        } else {
            if (a > 40) {
                return 2
            } else {
                return 1
            }
        }
    },
    pswdShow: function () {
        $(".y_regist_form").delegate(".y_set_password", "keyup", function () {
            liItem = $(this).parents("li");
            strenth = jRegist.checkStrong($(this));
            liItem.removeClass("ifocus");
            if (strenth == 1) {
                $(".y_regist_tips_keywords").hide();
                liItem.find(".strength_l").show()
            } else {
                if (strenth == 2) {
                    $(".y_regist_tips_keywords").hide();
                    liItem.find(".strength_m").show()
                } else {
                    if (strenth == 3) {
                        $(".y_regist_tips_keywords").hide();
                        liItem.find(".strength_h").show()
                    }
                }
            }
        })
    },
    loadingBtn: function () {
        $(".y_regist_form").delegate(".y_agreement_btn", "click", function () {
            if ($(this).hasClass("y_agreement_btn_loading")) {
                return false
            }
            jRegist.registerByPhoneSubmit();
            return false
        })
    },
    helpCenterHover: function () {
        $(".help_wrap", ".regist_header_right ").hover(function () {
            $(this).addClass("help_wrap_hover")
        }, function () {
            $(this).removeClass("help_wrap_hover")
        })
    },
    //提示信息框
    tipsShowHide: function () {
        $(".y_regist_tips").each(function (a) {
            var c = $("li", ".y_regist_form").height();
            var d = $(this).height();
            var b = (c - d) / 2;
            $(this).css("top", b);
            jRegist.maxWidth(this, 160)
        })
    },
    maxWidth: function (c, d) {
        if (jRegist.ieLower) {
            var b = $(c).width();
            var a = parseInt(d);
            if (b > a) {
                $(c).width(a)
            }
        }
    },
    messageBind: function () {
        // 用户名input失焦
        $("#userName").blur(function () {
            if (registCheck.check_account_regx($(this).val()) == 0) {
                jRegist.is_account_register()
            }
        });
        //用户名input聚焦
        $("#userName").focus(function () {
            tip.show_default_message($(this), "4-20位字符，可由中文、英文、数字或符号“_”组成")
        });
        //手机号input框变化事件
        $("#phone").change(function () {
            sms.change_state_disable();
            if (registCheck.check_phone_regx($(this).val())) {
                sms.change_state_require();
                tip.hide_error_message($("#phone"))
                console.log("yes");
                // jRegist.is_phone_register()
            }
        });
        $("#phone").keyup(function () {
            if (!registCheck.check_phone_regx($(this).val())) {
                tip.show_default_message($(this), "请填写正确的手机号码，以便 接收订单通知，找回密码等")
            } else {
                $(this).parents("li").removeClass("ifocus")
            }
        });
        $("#phone").blur(function () {
            if (!registCheck.check_phone_regx($(this).val())) {
                tip.show_error_message($(this), "格式错误，请输入正确的手机号码")
            } else {
                if (!$(this).parents("li").hasClass("ipt_wrong")) {
                    $(this).parents("li").removeClass("ifocus")
                }
            }
        });
        //聚焦
        $("#phone").focus(function () {
            if (!$(this).parents("li").hasClass("ipt_wrong") && !$(this).parents("li").hasClass("ipt_right")) {
                tip.show_default_message($(this), "请填写正确的手机号码，以便 接收订单通知，找回密码等")
            }
        });
        //获取验证码按钮点击事件
        $(".receive_code").click(function () {
            sms.change_state_resend();
            sms.start_time_count()
            jRegist.send_receive_code()
        });
    },
    // 检查用户名是否注册 -- lyq
    is_account_register: function () {
        $.ajax({
            type: "POST",
            url: "/wx/checkName",
            dataType: 'text',
            contentType:"application/json;charset=UTF-8",
            data: JSON.stringify({
                user_name:$("#userName").val()
            }),
            success: function (data) {
                console.log(data)
                if (data != '1') {
                    tip.show_error_message($("#userName"), "您输入的用户名已存在，请重新输入")
                } else {
                    tip.hide_error_message($("#userName"))
                }
            }
        })
    },
    // 检查手机号是否已经注册接口 -- lyq
    is_phone_register: function () {
        if (registerValidateUserBehaviorSwitcher) {
            showValidCodeWhenRegistByMobile = false
        }
        if (!registCheck.check_phone_regx($("#phone").val())) {
            tip.show_error_message($("#phone"), "格式错误，请输入正确的手机号码");
            return
        }
        $.ajax({
            type: "POST",
            url: "/wx/phone",
            contentType:"application/json;charset=UTF-8",
            data: JSON.stringify({
                user_phone:$("#phone").val()
            }),
            dataType:'text',
            success: function (b) {
                sms.reset_timer_state();
                //如果手机号已经存在
                if (b == '0') {
                    sms.change_state_disable();
                    tip.show_error_message($("#phone"), "该手机号已存在，<a href='D:/lyq/lyq/YHD_16852063/passport/login.html'>登录</a>");
                }else if (b.code == '1') {
                    sms.change_state_require();
                    tip.hide_error_message($("#phone"))
                }
            }
        })
    },
    //注册
    registerByPhoneSubmit: function () {
        $(".y_agreement_btn").addClass("y_agreement_btn_loading");
        $(".y_agreement_btn").text("注册中");
        var c = $("#userName").val();
        var e = $("#phone").val();
        var g = $("#password").val();
        var d = $("#password2").val();
        var a = $("#validPhoneCode").val();
        if (registCheck.check_account_regx(c) != 0) {
            tip.show_error_message($("#userName"), "格式错误，请输入正确的用户名");
            jRegist.phone_button_recover();
            return
        }
        if (!registCheck.check_phone_regx(e)) {
            tip.show_error_message($("#phone"), "格式错误，请输入正确的手机号");
            jRegist.phone_button_recover();
            return
        }
        if (!registCheck.check_password_regx()) {
            jRegist.phone_button_recover();
            return
        }
        if (g != d) {
            tip.show_error_message($("#password2"), "两次密码输入不一致");
            jRegist.phone_button_recover();
            return
        }
        if (SCaptcha.isSlide() && SCaptcha.isShowing()) {
            jRegist.phone_button_recover();
            jRegist.show_slide_captcha();
            return
        }
        if (a.length != 6) {
            tip.show_error_message($("#validPhoneCode"), "请输入6位短信验证码");
            jRegist.phone_button_recover();
            return
        }
        // c = encrypt.encrypt(c);
        // e = encrypt.encrypt(e);
        // g = encrypt.encrypt(g);
        // d = encrypt.encrypt(d);
        var b = {
            userName: c,
            validPhone: e,
            "user.password": g,
            password2: d,
            validCode: a,
            returnUrl: $("#returnUrl").val()
        };
        var f = "/wx/zhuce";
        $.ajax({
            type: "POST",
            url: f,
            data: JSON.stringify({
                user_name: c,
                user_phone: e,
                user_password: d,
                user_id: 1
            }),
            contentType:"application/json;charset=UTF-8",
            success: function (k) {
                if (k == "1") {
                    alert("注册成功");
                    setTimeout(function () {
                        jRegist.phone_button_recover
                        window.location.href = "../passport/login.html"
                    }, 1000);
                } else if (k == "0") {
                    alert("注册失败");
                    jRegist.phone_button_recover()
                } else {
                    alert("系统繁忙，请稍后再试");
                }
            }
        })
    },
    //发送短信验证码接口--lyq
    send_receive_code: function () {
        // if (!sms.is_send_able()) {
        //     if (SCaptcha.isSlide() && $(".receive_code").hasClass("r_disable_code")) {
        //         SCaptcha.show()
        //     }
        //     return false
        // } else {
        //     SCaptcha.disSlide();
            $.ajax({
                type: "POST",
                url: "/wx/phone",
                async: true,
                data: JSON.stringify({
                    user_phone:$("#phone").val()
                }),
                dataType:'text',
                contentType:"application/json;charset=UTF-8",
                success: function (c) {
                    console.log(c)
                    if (c == '0') {
                        tip.show_error_message($("#phone"), "请输入正确的手机号码");
                    } else {
                        phoneCode = c;
                    }
                },
                error:function () {
                    alert("系统繁忙，请稍后再试");
                }
            })
        // }
    },
    //恢复注册按钮
    phone_button_recover: function () {
        $(".y_agreement_btn").removeClass("y_agreement_btn_loading");
        $(".y_agreement_btn").text("同意协议并注册")
    },
    init_slide_captcha: function () {
        var a = {
            env: "pc-register",
            registBtn: "register_button",
            beforeCheck: function () {
                return registCheck.check_phone_regx($("#phone").val())
            },
            hidePicCaptcha: function () {
                $("#validCodeDiv").hide()
            },
            onValidateSuccess: function () {
                sms.change_state_require();
                jRegist.send_receive_code(sms.auto_send_receive_callback)
            }
        };
        SCaptcha = new SlideCaptcha(a)
    },
}
registCheck = {
    //检查用户名合法性
    check_account_regx: function (a) {
        if (a == "") {
            tip.show_error_message($("#userName"), "用户名不能为空");
            return 1
        }
        if (a.length > 20 || a.length < 4) {
            tip.show_error_message($("#userName"), "请输入正确的用户名,用户名应为4-20位字符");
            return 2
        }
        if (/^\d+$/.test(a)) {
            tip.show_error_message($("#userName"), "请输入正确的用户名,用户名不能全为数字");
            return 3
        }
        if (!/^[\u4E00-\u9FA5a-zA-Z0-9_]+$/.test(a)) {
            tip.show_error_message($("#userName"), "用户名格式错误,请输入正确的用户名");
            return 4
        }
        return 0
    },
    //检查手机号合法性
    check_phone_regx: function (a) {
        if (a == null || a == "") {
            return false
        }
        var b = /^1[0-9]{10}$/;
        if (!b.test(a)) {
            return false
        }
        return true
    },
    // 检查密码规范性
    check_password_regx: function () {
        var a = check_pwd1("password");
        switch (a) {
            case 0:
                tip.hide_error_message($("#password"));
                return true;
            case 1:
                tip.show_error_message($("#password"), "密码不能为空");
                break;
            case 2:
                tip.show_error_message($("#password"), "密码应为6-20位字符");
                break;
            case 3:
                tip.show_error_message($("#password"), "密码应为6-20位字符");
                break;
            case 4:
                tip.show_error_message($("#password"), "密码中不允许有空格");
                break;
            case 5:
                tip.show_error_message($("#password"), "密码不能全为数字");
                break;
            case 6:
                tip.show_error_message($("#password"), "密码不能全为字母，请包含至少1个数字或符号 ");
                break;
            case 7:
                tip.show_error_message($("#password"), "密码不能全为符号");
                break;
            case 8:
                tip.show_error_message($("#password"), "密码不能全为相同字符或数字");
                break;
            default:
                tip.show_error_message($("#password"), "6-20个大小写英文字母、符号或数字的组合")
        }
        $(".y_regist_tips_keywords").hide();
        return false
    },
    // 检查密码一致性
    check_password2: function () {
        var b = $("#password").val();
        var a = $("#password2").val();
        if (registCheck.check_password_regx()) {
            if (b != a) {
                tip.show_error_message($("#password2"), "两次密码输入不一致")
            } else {
                tip.hide_error_message($("#password2"))
            }
        }
    },
    is_input_error: function (a) {
        if (a.parents("li").hasClass("ipt_wrong")) {
            return true
        }
        return false
    }
};
tip = {
    //提示错误信息
    show_error_message: function (a, b) {
        a.parents("li").addClass("ipt_wrong");
        a.parents("li").removeClass("ipt_right");
        a.parents("li").find(".y_regist_tips").removeClass("y_regist_tips_black");
        a.parents("li").find(".y_regist_tips").addClass("y_regist_tips_red");
        if (b != null) {
            a.parents("li").find(".y_tips_words").first().html(b);
            tip.message_fix(a);
            a.parents("li").addClass("ifocus")
        }
    },
    //隐藏错误信息并显示对勾图标（ipt_right）
    hide_error_message: function (a) {
        a.parents("li").removeClass("ipt_wrong");
        a.parents("li").addClass("ipt_right");
        a.parents("li").find(".y_regist_tips").removeClass("y_regist_tips_red");
        a.parents("li").removeClass("ifocus");
        tip.message_fix2(a)
    },
    //显示默认提示信息
    show_default_message: function (a, b) {
        a.parents("li").removeClass("ipt_wrong");
        a.parents("li").removeClass("ipt_right");
        if (b != null) {
            a.parents("li").find(".y_regist_tips").removeClass("y_regist_tips_red");
            a.parents("li").find(".y_regist_tips").addClass("y_regist_tips_black");
            a.parents("li").find(".y_tips_words").first().html(b);
            tip.message_fix(a);
            a.parents("li").addClass("ifocus")
        }
    },
    message_fix: function (f) {
        var b = $("li", ".y_regist_form").height();
        var e = f.parents("li").find(".y_regist_tips").height();
        var i = (b - e) / 2;
        f.parents("li").find(".y_regist_tips").css("top", i);
        var g = !-[1];
        var a = document.documentMode;
        var j = !!window.ActiveXObject;
        var k = j && a == 9;
        if (g || k) {
            var d = f.parents("li").find(".y_regist_tips");
            var c = d.outerWidth(true);
            var h = d.outerHeight(true);
            if (d) {
                d.stop().animate({
                    opacity: 1,
                    width: c,
                    height: h
                }, 300, function () {
                })
            }
        }
    },
    message_fix2: function (d) {
        var c = !-[1];
        var f = document.documentMode;
        var g = !!window.ActiveXObject;
        var a = g && f == 9;
        if (c || a) {
            var b = d.parents("li").find(".y_regist_tips");
            var e = b.outerWidth(true);
            var h = b.outerHeight(true);
            if (b) {
                b.stop().animate({
                    opacity: 0,
                    width: 0,
                    height: 0
                }, 300, function () {
                    b.css({
                        width: e,
                        height: h
                    })
                })
            }
        }
    }
};
sms = {
    send_receive_callback: function () {
        console.log(11)
        sms.change_state_resend();
        sms.start_time_count()
    },
    auto_send_receive_callback: function () {
        $("#validCodeDiv").hide();
        $("#validPhoneCodeDiv").show();
        sms.change_state_resend();
        sms.start_time_count()
    },
    is_send_able: function () {
        if ($(".receive_code").hasClass("r_resend_code")) {
            return false
        }
        if ($(".receive_code").hasClass("r_disable_code")) {
            return false
        }
        return true
    },
    change_state_disable: function () {
        $(".receive_code").removeClass("r_require_code");
        $(".receive_code").removeClass("r_resend_code");
        $(".receive_code").addClass("r_disable_code");
        $(".receive_code").html("<span>获取验证码</span>")
    },
    // 可以获取验证码
    change_state_require: function () {
        $(".receive_code").addClass("r_require_code");
        $(".receive_code").removeClass("r_resend_code");
        $(".receive_code").removeClass("r_disable_code");
        $(".receive_code").html("<span>获取验证码</span>")
    },
    change_state_resend: function () {
        $(".receive_code").removeClass("r_require_code");
        $(".receive_code").addClass("r_resend_code");
        $(".receive_code").removeClass("r_disable_code");
        $(".receive_code").html("重新发送(<i>60</i>)")
    },
    reset_timer_state: function () {
        if (timer1 != null) {
            timer1.stop()
        }
        if (timer2 != null) {
            timer2.stop()
        }
    },
    //倒计时
    start_time_count: function () {
        var b = function () {
            var c = $("i", ".same_code_btn").text();
            if (c > 0) {
                c--;
                $("i", ".same_code_btn").text(c)
            }
        };
        sms.reset_timer_state();
        timer1 = new Timer();
        timer1.startInterval({}, b);
        var a = function () {
            sms.change_state_require()
        };
        timer2 = new Timer();
        timer2.start({}, a)
    },
    hide_audio_msg: function () {
        $("#validPhoneCode").next(".y_same_label").text("短信验证码");
        tip.hide_error_message($("#validPhoneCode"))
    }
};
var checkPassword = {
    pwdObject: {
        pwd: "",
        pwdLength: 0,
        upperAlpCount: 0,
        lowerAlpCount: 0,
        numCount: 0,
        charCount: 0
    },
    getPwdPoint: function (b) {
        this.pwdObject.pwd = b.replace("\\s", "");
        this.pwdObject.pwdLength = this.pwdObject.pwd.length;
        var a = 0;
        a += this.pwdLengthCheck();
        a += this.pwdUpperCheck();
        a += this.pwdLowerCheck();
        a += this.pwdNumCheck();
        a += this.pwdCharCheck();
        a += this.pwdLowerRequest();
        a += this.pwdOnlyHasAlp();
        a += this.pwdOnlyHasNum();
        a += this.pwdSameLetterCheck();
        a += this.pwdSeriseUpperAlp();
        a += this.pwdSeriseLowerAlp();
        a += this.pwdSeriseNum();
        a += this.pwdSeriseAlp2Three();
        a += this.pwdSeriseNum2Three();
        return a
    },
    pwdLengthCheck: function () {
        return this.pwdObject.pwdLength * 4
    },
    pwdUpperCheck: function () {
        var b = 0;
        var a = this.pwdObject.pwd.match(/[A-Z]/g);
        if (a) {
            b = a.length
        }
        this.pwdObject.upperAlpCount = b;
        if (b <= 0) {
            return 0
        }
        return (this.pwdObject.pwdLength - b) * 2
    },
    pwdLowerCheck: function () {
        var b = 0;
        var a = this.pwdObject.pwd.match(/[a-z]/g);
        if (a) {
            b = a.length
        }
        this.pwdObject.lowerAlpCount = b;
        if (b <= 0) {
            return 0
        }
        return (this.pwdObject.pwdLength - b) * 2
    },
    pwdNumCheck: function () {
        var b = 0;
        var a = this.pwdObject.pwd.match(/[0-9]/g);
        if (a) {
            b = a.length
        }
        this.pwdObject.numCount = b;
        if (b == this.pwdObject.pwdLength) {
            return 0
        }
        return this.pwdObject.numCount * 2
    },
    pwdCharCheck: function () {
        this.pwdObject.charCount = this.pwdObject.pwdLength - this.pwdObject.upperAlpCount - this.pwdObject.lowerAlpCount - this.pwdObject.numCount;
        return this.pwdObject.charCount * 6
    },
    pwdLowerRequest: function () {
        var a = 0;
        if (this.pwdObject.pwdLength >= 6) {
            a++
        }
        if (this.pwdObject.upperAlpCount > 0) {
            a++
        }
        if (this.pwdObject.lowerAlpCount > 0) {
            a++
        }
        if (this.pwdObject.numCount > 0) {
            a++
        }
        if (this.pwdObject.charCount > 0) {
            a++
        }
        if (a < 4) {
            a = 0
        }
        return a * 2
    },
    pwdOnlyHasAlp: function () {
        if (this.pwdObject.pwdLength == (this.pwdObject.upperAlpCount + this.pwdObject.lowerAlpCount)) {
            return -this.pwdObject.pwdLength
        }
        return 0
    },
    pwdOnlyHasNum: function () {
        if (this.pwdObject.pwdLength == this.pwdObject.numCount) {
            return -this.pwdObject.pwdLength
        }
        return 0
    },
    pwdSeriseUpperAlp: function () {
        var d = 0;
        var c = this.pwdObject.pwd;
        var b = /[A-Z]/;
        for (var a = 0; a < c.length - 1; a++) {
            if (b.test(c[a]) && b.test(c[a + 1])) {
                d++
            }
        }
        return -2 * d
    },
    pwdSeriseLowerAlp: function () {
        var d = 0;
        var c = this.pwdObject.pwd;
        var b = /[a-z]/;
        for (var a = 0; a < c.length - 1; a++) {
            if (b.test(c[a]) && b.test(c[a + 1])) {
                d++
            }
        }
        return -2 * d
    },
    pwdSeriseNum: function () {
        var d = 0;
        var c = this.pwdObject.pwd;
        var b = /[0-9]/;
        for (var a = 0; a < c.length - 1; a++) {
            if (b.test(c[a]) && b.test(c[a + 1])) {
                d++
            }
        }
        return -2 * d
    },
    pwdSeriseAlp2Three: function () {
        var d = 0;
        var c = this.pwdObject.pwd.toLocaleLowerCase();
        var b = /[a-z]/;
        for (var a = 0; a < c.length - 2; a++) {
            if (b.test(c[a]) && (c.charCodeAt(a + 1) == (c.charCodeAt(a) + 1)) && (c.charCodeAt(a + 2) == (c.charCodeAt(a) + 2))) {
                d++
            }
        }
        return -3 * d
    },
    pwdSeriseNum2Three: function () {
        var d = 0;
        var c = this.pwdObject.pwd;
        var b = /[0-9]/;
        for (var a = 0; a < c.length - 2; a++) {
            if (b.test(c[a]) && (c.charCodeAt(a + 1) == (c.charCodeAt(a) + 1)) && (c.charCodeAt(a + 2) == (c.charCodeAt(a) + 2))) {
                d++
            }
        }
        return -3 * d
    },
    pwdSameLetterCheck: function () {
        var d = {};
        var c = this.pwdObject.pwd.toLocaleLowerCase();
        for (var f = 0; f < c.length; f++) {
            if (d[c[f]]) {
                d[c[f]]++
            } else {
                d[c[f]] = 1
            }
        }
        var a = 0;
        for (var b in d) {
            var e = d[b];
            if (e > 0) {
                a += e * (e - 1)
            }
        }
        return -a
    }
};
function Timer() {
    this.timer = null;
    this.startInterval = function (c, b) {
        var a = function () {
        };
        if (typeof b == "function") {
            a = b
        }
        this.timer = setInterval(a, 1000)
    };
    this.start = function (c, b) {
        var a = function () {
        };
        if (typeof b == "function") {
            a = b
        }
        this.timer = setTimeout(a, 60 * 1000)
    };
    this.stop = function () {
        if (this.timer != null) {
            clearInterval(this.timer);
            this.timer = null
        }
    }
}
function check_pwd1(d) {
    var b = $("#" + d).val();
    if (b == "") {
        return 1
    }
    if (b.length > 20) {
        return 2
    }
    if (b.length < 6) {
        return 3
    }
    var e = /\s+/;
    if (e.test(b)) {
        return 4
    }
    var g = /^[0-9]+$/;
    if (g.test(b)) {
        return 5
    }
    var h = /^[a-zA-Z]+$/;
    if (h.test(b)) {
        return 6
    }
    var c = /^[^0-9A-Za-z]+$/;
    if (c.test(b)) {
        return 7
    }
    if (isSameWord(b)) {
        return 8
    }
    var a = "^[\\da-zA-Z\\.\\,\\`\\~\\!\\@\\#\\$\\%\\\\^\\&\\*\\(\\)\\-\\_\\=\\+\\[\\{\\]\\}\\\\|\\;\\:\\'\\'\\\"\\\"\\<\\>\\/\\?]+$";
    var f = new RegExp(a);
    if (!f.test(b)) {
        return 10
    }
    return 0
}
function isSameWord(b) {
    var d;
    if (b != null && b != "") {
        d = b.charCodeAt(0);
        d = "\\" + d.toString(8);
        var c = "[" + d + "]{" + (b.length) + "}";
        var a = new RegExp(c);
        return a.test(b)
    }
    return true
}
