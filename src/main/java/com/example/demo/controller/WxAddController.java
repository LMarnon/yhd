package com.example.demo.controller;

import com.example.demo.entity.NameEntity;
import com.example.demo.entity.PhoneEntity;
import com.example.demo.entity.sql.WxLofinEntity;
import com.example.demo.entity.sql.WxUserInfoEntity;
import com.example.demo.service.WxAddService;
import com.example.demo.service.run.ALiYunVoice;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RestController
@Controller
@RequestMapping("/wx")
public class WxAddController {
    @Autowired
    WxAddService wxAddService;
    @Autowired
    ALiYunVoice aLiYunVoice;
    /**
     *注册，验证账户
     */
    @PostMapping("/checkName")
    public String checkUserName(@Validated @RequestBody NameEntity nameEntity){
        return wxAddService.isUserName(nameEntity.getUser_name());
    }
    /**
     *注册
     */
    @PostMapping("/zhuce")
    public String addUserInfo(@Validated @RequestBody WxUserInfoEntity wxUserInfoEntity){
          return wxAddService.insertWxInfo(wxUserInfoEntity);
    }
    /**
     *登陆
     */
    @PostMapping("/login")
    public String userLogin(@Validated @RequestBody WxLofinEntity wxLofinEntity){
        return wxAddService.selectWxInfo(wxLofinEntity);
    }
    /**
     *找回密码，查询用户名
     */
    @PostMapping("/find")
    public String findPassword(@Validated @RequestBody NameEntity nameEntity){
        return wxAddService.selectWxByUserName(nameEntity.getUser_name());
    }
    /**
     *发语音
     */
    @PostMapping("/phone")
    public String sendPhone(@Validated @RequestBody PhoneEntity phoneEntity){
        System.out.println(phoneEntity.getUser_phone());
        return aLiYunVoice.sendVoice(phoneEntity.getUser_phone());
    }
    /**
     *更新密码
     */
    @PostMapping("/upUser")
    public String upUser(@Validated @RequestBody WxLofinEntity wxLofinEntity){
        return wxAddService.upUserByUserName(wxLofinEntity);
    }

}
