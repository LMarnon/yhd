package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletResponse;

@Controller
public class ReController {

    //系统首页
    @RequestMapping("/")
    public String index(Model model, HttpServletResponse response) {
        return "/jsp/index";
    }

    //跳转到主页面/main.jsp
    @RequestMapping(value = "/main", method = RequestMethod.GET)
    public String toMain() {
        return "/jsp/main";
    }

    //跳转到登录页面/login.jsp
    @RequestMapping("/login")
    public String login(Model model) {
        return "/jsp/login";
    }

    //跳转到注册页面/register.jsp
    @RequestMapping("/register")
    public String register(Model model) {
        return "/jsp/register";
    }

    //跳转到修改密码页面页面/forgetPassword.jsp
    @RequestMapping("/forgetPassword")
    public String forgetPassword(Model model) {
        return "/jsp/forgetPassword";
    }
}
