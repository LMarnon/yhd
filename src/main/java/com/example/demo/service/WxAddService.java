package com.example.demo.service;

import com.example.demo.entity.sql.WxUserInfoEntity;
import com.example.demo.entity.sql.WxLofinEntity;

import java.util.List;

public interface WxAddService {
    /**
     *添加信息
     */
    String insertWxInfo(WxUserInfoEntity wxAddEntity);
    /**
     *查询信息
     */
    String selectWxInfo(WxLofinEntity wxLofinEntity);
    /**
     *查询账户是否存在,并发送语音
     */
    String selectWxByUserName(String user_name);
    /**
     *按用户更改密码
     * @param wxLofinEntity
     */
    String upUserByUserName(WxLofinEntity wxLofinEntity);
    /**
     *查询账户是否存在
     */
    String isUserName(String user_name);
}
