package com.example.demo.service.run;

import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import sun.rmi.runtime.Log;

@Slf4j
@Service("ALiYunVoice")
public class ALiYunVoice {
    public String sendVoice(String phone) {
        DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", "LTAI4FrDG5Enr6Hqr6Fs35cC", "");
//        q2NAyqP30ILJb1J2Vn4mxBGI1pFCWB
        IAcsClient client = new DefaultAcsClient(profile);
        Integer i = (int)((Math.random()*(999999-100000+1)));
        String code = i.toString();
        CommonRequest request = new CommonRequest();
        request.setMethod(MethodType.POST);
        request.setDomain("dysmsapi.aliyuncs.com");
        request.setVersion("2017-05-25");
        request.setAction("SendSms");
        request.putQueryParameter("RegionId", "cn-hangzhou");
        request.putQueryParameter("PhoneNumbers",phone.trim());
        request.putQueryParameter("SignName", "小小工程狮");
        request.putQueryParameter("TemplateCode", "SMS_175540471");
        request.putQueryParameter("TemplateParam", "{\"code\":"+code+"}");
        log.info("coed = "+code);
        try {
            CommonResponse response = client.getCommonResponse(request);
            System.out.println(response.getData());
            return code;
        } catch (ServerException e) {
            e.printStackTrace();
        } catch (ClientException e) {
            e.printStackTrace();
        }finally {
            return null;
        }
        return code;
    }
}
