package com.example.demo.service.impl;

import com.example.demo.entity.sql.WxUserInfoEntity;
import com.example.demo.entity.sql.WxLofinEntity;
import com.example.demo.mapper.WxAddMapper;
import com.example.demo.service.WxAddService;
import com.example.demo.service.run.ALiYunVoice;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service("wxAddService")
public class WxAddServiceImpl implements WxAddService {
    @Autowired
    WxAddMapper wxAddMapper;
    @Autowired
    ALiYunVoice aLiYunVoice;
    /**
     *注册
     */
    @Override
    public String insertWxInfo(WxUserInfoEntity wxUserInfoEntity) {
        try{

            List<WxUserInfoEntity> list = wxAddMapper.selectWxUserName(wxUserInfoEntity.getUser_name());
            if(list.size() == 0){
                wxAddMapper.insertWxInfo(wxUserInfoEntity);
                return  "1";
            }
            return  "0";
        }catch (Exception e){
            log.error(e.getMessage());
            return "0";
        }
    }

    /**
     *登陆
     */
    @Override
    public String selectWxInfo(WxLofinEntity wxLofinEntity) {
        System.out.println(wxLofinEntity.getUser_name());
        log.info(wxLofinEntity.getUser_password());
        List<WxUserInfoEntity> wxUserInfoEntityList = wxAddMapper.selectWxInfo(wxLofinEntity);
        if (wxUserInfoEntityList.size() == 0){
            return "0";
        }
        return "1";
    }

    /**
     *找回
     */
    @Override
    public String selectWxByUserName(String user_name) {
        List<WxUserInfoEntity> list = wxAddMapper.selectWxUserName(user_name);
        if(list.size() == 0){
            return "0";
        }
        return aLiYunVoice.sendVoice( list.get(0).getUser_phone());
    }

    /**
     *按照用户名更改密码
     */
    @Override
    public String upUserByUserName(WxLofinEntity wxLofinEntity) {
        if(wxAddMapper.upUserByUserName(wxLofinEntity) == 0){
            return "0";
        }
        return "1";
    }

    @Override
    public String isUserName(String user_name) {
        List<WxUserInfoEntity> list = wxAddMapper.selectWxUserName(user_name);
        if(list.size() == 0){
            return "1";
        }
        return  "0";
    }
}
